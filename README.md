# Summative 5

| API                              |
| -------------------------------- |
| **GET**/movie/popular            |
| **GET**/movie/{movie_id}         |
| **GET**/movie/{movie_id}/credits |
| **GET**/movie/{movie_id}/images  |
| **GET**/movie/{movie_id}/videos  |
| **GET**/movie/now_playing        |
| **GET**/movie/top_rated          |

**Screenshoot**

![](./gambar/5.jpg)

![](./gambar/6.jpg)

![](./gambar/1.jpg)

![](./gambar/2.jpg)

![](./gambar/3.jpg)

![](./gambar/4.jpg) 
