import { Fragment } from "react";
import { useSelector } from "react-redux";
import Header from "./components/Header";
import Dashboard from "./components/Dashboard";
import { Route, Routes } from "react-router-dom";
import LoginPage from "./components/LoginPage";
import ShowMovie from "./components/ShowMovie";
import NowPlaying from "./components/NowPlaying";
import TopRated from "./components/TopRated";

function App() {
  const isAuth = useSelector((state) => {
    return state.cs.isAuth;
  });

  return (
    <Fragment>
      {isAuth ? (
        <>
          <Header />
          <Routes>
            <Route path="/" element={<Dashboard/>}/>
            <Route path="/showMovie/:movie_id" element={<ShowMovie/>}/>
            <Route path="/nowplaying" element={<NowPlaying/>}/>
            <Route path="/toprated" element={<TopRated/>}/>
          </Routes>
        </>
      ) : (
        <LoginPage />
      )}
    </Fragment>
  );
}

export default App;
