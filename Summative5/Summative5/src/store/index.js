import { configureStore } from "@reduxjs/toolkit";

import LoginPage from "./loginPage";
import movies from "./movies";

const store = configureStore({
  reducer: {
    cs: LoginPage,
    movies: movies
  },
});

export default store;
