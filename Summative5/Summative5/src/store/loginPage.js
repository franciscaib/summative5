import { createSlice } from "@reduxjs/toolkit";
import swal from "sweetalert";

const initialState = {
  isAuth: false,
  isError: null,
};

const counterSlices = createSlice({
  name: "cs",
  initialState: initialState,
  reducers: {
    login(state, data) {
      if (data.payload.username === 'intan') {
        state.isAuth = true;
      } else {
        swal({
          title : 'Login Fail',
          text :'Username or Password must be filled',
          icon :'error'
      })
      }
    },
    logout(state) {
      state.isAuth = false;
    },
  },
});

export const counterActions = counterSlices.actions;

export default counterSlices.reducer;
