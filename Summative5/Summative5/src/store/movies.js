import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listMovies: [],
  moviebyId:[],
  listNowPlaying:[],
  topRated:[],
  movieCredit:[],
  videos:[],
};

const moviesSlices = createSlice({
  name: "movies",
  initialState: initialState,
  reducers: {
    getAllMovies(state, data) {
      state.listMovies = data.payload;
    },
    getMoviesById(state, data){
      state.moviebyId = data.payload;
    },
    getNowPlaying(state,data){
      state.listNowPlaying = data.payload
    }, 
    getTopRated(state,data){
      state.topRated = data.payload
    },
    getCredit(state,data){
      state.movieCredit = data.payload
    },
    getVideos(state,data){
      state.videos = data.payload
    }
  },
});

export const moviesActions = moviesSlices.actions;

export default moviesSlices.reducer;
