import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { counterActions } from "../store/loginPage";
import classes from "./Header.module.css";
import {Button} from "react-bootstrap";
import { Link } from "react-router-dom";

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const logoutHandler = () => {
    navigate("/");
    dispatch(counterActions.logout());
  };

  const linkHandler=()=>{
    navigate("/nowplaying");
  }

  const logoHandler=()=>{
    navigate("/");
  }
  const linkTopHandler=()=>{
    navigate("/toprated");
  }
  
  return (
    <header className={classes.header}>
      <h1 onClick={logoHandler} style={{cursor:"pointer", color:"white"}}>TMDB</h1>
      <nav>
        <ul>
          <li>
            <a onClick={linkTopHandler} style={{cursor:"pointer", color:"white"}}>Top Rated</a>
          </li>
          <li>
            <a onClick={linkHandler} style={{cursor:"pointer", color:"white"}}>Now Playing</a>
          </li>
          <li>
          
            <Button onClick={logoutHandler} className="customButton">Logout</Button>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
