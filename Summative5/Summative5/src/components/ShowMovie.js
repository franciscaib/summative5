import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Container, Row, Card, Col } from "react-bootstrap";
import { moviesActions } from "../store/movies";
import { useState } from "react";

const ShowMovie = () => {
    const moviebyId = useSelector((state) => state.movies.moviebyId);
    const movieCredit = useSelector((state) => state.movies.movieCredit);
    const videos = useSelector((state) => state.movies.videos);
    const dispatch = useDispatch();
    const [genre, setGenre] = useState([]);

    const params = useParams();
    useEffect(() => {
        axios.get(
            `https://api.themoviedb.org/3/movie/${params.movie_id}?api_key=0b1aad01235d696060020ec9eab64b28&language=en-US`
        )
            .then(resJson => {
                dispatch(moviesActions.getMoviesById(resJson.data))
                setGenre(resJson.data.genres);
            })

        axios.get(
            `https://api.themoviedb.org/3/movie/${params.movie_id}/credits?api_key=0b1aad01235d696060020ec9eab64b28&language=en-US`
        )
            .then(resJson => {
                dispatch(moviesActions.getCredit(resJson.data))
            })

        axios.get(
            `https://api.themoviedb.org/3/movie/${params.movie_id}/videos?api_key=0b1aad01235d696060020ec9eab64b28&language=en-US`
        )
        .then(resJson => {
            dispatch(moviesActions.getVideos(resJson.data.results))
        })
    }, [dispatch]);

    console.log(moviebyId);
    return (
        <div style={{ backgroundColor: "silver" }}>
            <Container>
                <br />
                <Row>
                    <Col>
                        <Card.Img variant="top"
                            style={{
                                height: "auto",
                                width: "76%",
                                margin: "10px",
                                float: "right"
                            }}
                            src={`http://image.tmdb.org/t/p/w500${moviebyId.poster_path}`}
                        />
                    </Col>
                    <Col >
                        <Card.Title>
                            <h2>
                                {moviebyId.original_title}
                            </h2>
                            <p style={{ fontSize: "15px" }}>{moviebyId.release_date}
                            </p>
                        </Card.Title>
                        <Card.Text>
                            <h4 style={{ color: "teal" }}>Overview</h4>
                            <p>{moviebyId.overview}</p>
                        </Card.Text>
                        <Card.Text>
                            <h4 style={{ color: "teal" }}>Genre</h4>
                            <ul>
                                {genre.map((value) => {
                                    return (
                                        <li style={{ display: "inline" }}>{value.name} </li>
                                    )
                                })}
                            </ul>
                        </Card.Text>
                        <iframe width="420" height="315" 
                        src={`https://www.youtube.com/embed/${videos[0].key}`} 
                        frameborder="0" allowfullscreen></iframe>
                    </Col>
                </Row>
            </Container>
            <hr />
            <center>
                <Container fluid>
                    <Row className="g-4 card-example d-flex flex-row flex-nowrap overflow-auto">
                        {movieCredit.cast.map((value) => {
                            return (
                                <Col>
                                    <Card key={value.cast_id} style={{ height: "23em", width: "13em" }}>
                                        <center>
                                        <img
                                            className="d-block w-100"
                                            src={`https://image.tmdb.org/t/p/w185${value.profile_path}`}
                                            style={{
                                                height: "auto",
                                                width: "200px",
                                            }}
                                        />
                                        </center>
                                        <Card.Title style={{ marginTop: "10px" }}>
                                            <p>{value.name}</p>
                                        </Card.Title>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </Container>
            </center>
        </div>
    )
}

export default ShowMovie;