import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { moviesActions } from "../store/movies";
import classes from "./UserProfile.module.css";
import { Card, Button, Row, Col, Container, Carousel } from "react-bootstrap";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Dashboard = () => {
  const listMovies = useSelector((state) => state.movies.listMovies);
  const dispatch = useDispatch();
  let navigate = useNavigate();
  useEffect(() => {
    axios.get(
      "https://api.themoviedb.org/3/movie/popular?api_key=0b1aad01235d696060020ec9eab64b28&language=en-US&page=1")
      .then(resJson => {
        dispatch(moviesActions.getAllMovies(resJson.data.results))
      })


  }, [dispatch]);

  console.log(listMovies);

  function buttonHandler(movieId) {
    navigate({ pathname: `/showMovie/${movieId}` })
  }


  return (
    <div style={{ backgroundColor: "silver" }}>
      <Carousel variant="dark" style={{ backgroundColor: "black" }}>
        {listMovies.map((value) => {
          return (
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={`http://image.tmdb.org/t/p/w500${value.poster_path}`}
                style={{
                  height: "auto",
                  width: "10px",
                  maxWidth: "200px"
                }}
              />
              <Carousel.Caption>
                <h5 style={{ color: "teal" }}>{value.overview}</h5>
              </Carousel.Caption>
            </Carousel.Item>
          )
        })}
      </Carousel>
      <br />
      <h5 className={classes.colorStyle}>What's popular</h5>
      <hr />
      <center>
        <Container fluid>
          <Row xs={1} md={6} className="g-4 card-example d-flex flex-row flex-nowrap overflow-auto">
            {listMovies.map((value) => {
              return (
                <Col>
                  <Card key={value.id} style={{ height: "23em", width: "13em" }}>
                    <Card.Img className={classes.imgSize}
                      variant="top"
                      style={{
                        height: "auto",
                        width: "90%",
                        margin: "10px",
                      }}
                      src={`http://image.tmdb.org/t/p/w500${value.poster_path}`}
                      onClick={() => buttonHandler(value.id)}
                    />

                    <Card.Title style={{ marginTop: "10px" }}>
                      <p>{value.original_title}</p>
                    </Card.Title>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </Container>
      </center>
    </div>







    /* <main className={classes.profile}>
      {listMovies.map((value) => {
        return (
          <div key={value.id}>
            <p>{value.original_title}</p>
            <img src={`http://image.tmdb.org/t/p/w500${value.poster_path}`} />
            <iframe
              src="https://www.youtube.com/embed/E7wJTI-1dvQ"
              frameborder="0"
              allow="autoplay; encrypted-media"
              allowfullscreen
              title="video"
            />
          </div>
          // https://www.youtube.com/watch?v=61800b461c6aa70043725b74
        );
      })}
      <p>TEST</p>
      {/* <h2>My User Profile</h2>
      <button>
        <Link to="/counter">Go To Counter</Link>
      </button> */
    // </main > 
  );
};

export default Dashboard;
