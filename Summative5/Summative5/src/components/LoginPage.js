import { useState } from "react";
import { useDispatch } from "react-redux";
import classes from "./LoginPage.module.css";
import { counterActions } from "../store/loginPage";
import { Form, Card, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const LoginPage = () => {
  const dispatch = useDispatch();
  const [form, setForm] = useState({
    username: "",
    password: "",
  });

  const formHandler = (events) => {
    return setForm({
      ...form,
      [events.target.name]: events.target.value,
    });
  };

  const loginHandler = (events) => {
    events.preventDefault();
    dispatch(counterActions.login(form))
  };

  return (
    <main className={classes.auth} style={{backgroundColor:"silver"}}>
      <section>
        <form onSubmit={loginHandler}>
          <center>
            <h1>Login</h1>
            <hr/>
          </center>
          <div className={classes.control}>
            <label htmlFor="username" className={classes.colorText}>Username</label>
            <input type="text" name="username" onChange={formHandler} />
          </div>
          <div className={classes.control}>
            <label htmlFor="password" className={classes.colorText}>Password</label>
            <input type="password" name="password" onChange={formHandler} />
          </div>
          <center>
            <br/>
          <button type="submit" style={{padding:"10px 25px"}}>Login</button>
          </center>
        </form>
      </section>
    </main>
  );
};

export default LoginPage;
