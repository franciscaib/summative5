import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import axios from "axios";
import { moviesActions } from "../store/movies";
import { Container, Row, Col, Card } from "react-bootstrap";
import classes from "./NovPlaying.module.css";
import { useNavigate } from "react-router";

function NowPlaying() {
    const listNowPlaying = useSelector((state) => state.movies.listNowPlaying);
    const dispatch = useDispatch();
    
  let navigate = useNavigate();

    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/movie/now_playing?api_key=0b1aad01235d696060020ec9eab64b28&language=en-US&page=1`)
            .then(resJson => {
                dispatch(moviesActions.getNowPlaying(resJson.data.results))
            });
    }, [dispatch]);

    function buttonHandler(movieId) {
        navigate({pathname:`/showMovie/${movieId}`})
      }

    return (
        <div style={{backgroundColor:"silver"}}>
            <br/>
        <h5 className={classes.colorStyle}>Now playing</h5>
        <hr/>
        <Container>
            <Row xs={1} md={6} className="g-4 ">
                {listNowPlaying.map((value) => {
                    return (
                        <Col>
                            <Card key={value.id} style={{ height: "20em", width: "11em" }}>
                                <Card.Img 
                                    variant="top"
                                    style={{
                                        height: "auto",
                                        width: "90%",
                                        margin: "10px",
                                    }}
                                    src={`http://image.tmdb.org/t/p/w500${value.poster_path}`}
                                    onClick={() => buttonHandler(value.id)}
                                />
                                <center>
                                <Card.Title style={{ marginTop: "10px" }}>
                                    <p>{value.original_title}</p>
                                </Card.Title>
                                </center>
                            </Card>
                        </Col>
                    )
                })}

            </Row>


        </Container>
        </div>
        
    )
}

export default NowPlaying;